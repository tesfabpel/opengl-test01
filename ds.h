//
// Created by tesx on 02/03/17.
//

#ifndef OPENGL_TEST01_DS_H
#define OPENGL_TEST01_DS_H

#include <glad/glad.h>

struct program_data_t
{
	GLint mvp_u;
	GLint tex_u;
	GLint color_u;
	
	GLint vertex_a;
	GLint uv_a;
};

#endif //OPENGL_TEST01_DS_H
