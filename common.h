#ifndef SIMCORE_COMMON_H
#define SIMCORE_COMMON_H

#pragma managed(push, off)

#include "main.h"
#include "ds.h"

namespace Simplify
{
	class Simplify;
}

namespace F4SimCore {

inline glm::ivec3 to_uintp(size_t i, const size_t n)
{
	const size_t z = i / (n * n);
	i %= n * n;
	
	const size_t y = i / n;
	i %= n;
	
	const size_t x = i;
	
	return { x, y, z };
}

void init_gl();

GLuint compile_and_link_shaders(const char *info_program_name,
	const char *vertex_src, 
	const char *fragment_src);

gl_program *compile_program(const char *program_name,
	const char *vertex_src, 
	const char *fragment_src,
	const std::vector<const char *> &uniforms);

void upload_to_vbos(const gl_mesh &mesh,
	GLuint vao_id,
	GLuint vbo_verts_id, 
	GLuint vbo_tris_id);

gl_mesh create_box(float sz_x, float sz_y, float sz_z);

void glClearErrors();

}

#pragma managed(pop)

#endif
