#include "main.h"
#include "common.h"
#include "shaders.h"
#include "gl_renderer.h"

namespace F4SimCore {

bool initialized = false;
void init_gl()
{
	if(initialized)
		return;

	if(!gladLoadGL())
	{
		exit(-1);
	}

	// create shaders
	gl_renderer::init();

	initialized = true;
}

void print_shader_infolog(GLuint shader)
{
	GLchar *log = static_cast<GLchar *>(malloc(sizeof(GLchar) * 1024));
	GLsizei logsz;
	glGetShaderInfoLog(shader, 1024, &logsz, log);
	fprintf(stderr, "%s\n", log);
	free(log);
}

void print_program_infolog(GLuint program)
{
	GLchar *log = static_cast<GLchar *>(malloc(sizeof(GLchar) * 1024));
	GLsizei logsz;
	glGetProgramInfoLog(program, 1024, &logsz, log);
	fprintf(stderr, "%s\n", log);
	free(log);
}

GLuint compile_and_link_shaders(const char *info_program_name,
	const char *vertex_src, 
	const char *fragment_src)
{
	GLint result;

	const GLuint sh_vertex = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(sh_vertex, 1, &vertex_src, nullptr);
	glCompileShader(sh_vertex);

	glGetShaderiv(sh_vertex, GL_COMPILE_STATUS, &result);
	if(result != GL_TRUE)
	{
		fprintf(stderr, "ERROR WHILE COMPILING PROGRAM %s\n", info_program_name);
		print_shader_infolog(sh_vertex);
		exit(-2);
	}

	const GLuint sh_fragment = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(sh_fragment, 1, &fragment_src, nullptr);
	glCompileShader(sh_fragment);

	glGetShaderiv(sh_fragment, GL_COMPILE_STATUS, &result);
	if(result != GL_TRUE)
	{
		fprintf(stderr, "ERROR WHILE COMPILING PROGRAM %s\n", info_program_name);
		print_shader_infolog(sh_fragment);
		exit(-2);
	}

	const GLuint program = glCreateProgram();
	glAttachShader(program, sh_vertex);
	glAttachShader(program, sh_fragment);
	glLinkProgram(program);

	glGetProgramiv(program, GL_LINK_STATUS, &result);
	if(result != GL_TRUE)
	{
		fprintf(stderr, "ERROR WHILE LINKING PROGRAM %s\n", info_program_name);
		print_program_infolog(program);
		exit(-3);
	}

	glDetachShader(program, sh_fragment);
	glDetachShader(program, sh_vertex);

	glDeleteShader(sh_fragment);
	glDeleteShader(sh_vertex);

	return program;
}

gl_program *compile_program(const char *program_name,
	const char *vertex_src, 
	const char *fragment_src, 
	const std::vector<const char *> &uniforms)
{
	const auto prog_id = compile_and_link_shaders(program_name,
		vertex_src,
		fragment_src);

	std::unordered_map<std::string, GLuint> map;

	for(const auto &uniform : uniforms)
	{
		const auto loc = glGetUniformLocation(prog_id, uniform);
		if(loc >= 0)
		{
			map[uniform] = GLuint(loc);
		}
	}

	auto prog = new gl_program();
	prog->program_id = prog_id;
	prog->program_name = program_name;
	prog->uniforms = std::move(map);
	return prog;
}

void upload_to_vbos(const gl_mesh &mesh,
	GLuint vao_id,
	GLuint vbo_verts_id,
	GLuint vbo_tris_id)
{
	glBindVertexArray(vao_id);

	// vertices
	if(vbo_verts_id != 0)
	{
		glBindBuffer(GL_ARRAY_BUFFER, vbo_verts_id);

		const size_t sz = mesh.vertices.size() * sizeof(GLfloat);
		glBufferData(GL_ARRAY_BUFFER, sz, mesh.vertices.data(), GL_STATIC_DRAW);

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

		glEnableVertexAttribArray(0);
	}

	// triangles
	if(vbo_tris_id != 0)
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_tris_id);

		const size_t sz = mesh.triangles.size() * sizeof(GLushort);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sz, mesh.triangles.data(), GL_STATIC_DRAW);
	}

	glBindVertexArray(0);
}



gl_mesh create_box(float sz_x, float sz_y, float sz_z)
{
	std::vector<GLfloat> verts = {
		0,0,0,
		1,0,0,
		1,0,1,
		0,0,1,
		0,1,0,
		1,1,0,
		1,1,1,
		0,1,1,
	};

	const std::vector<GLushort> tris = {
		3,2,1,
		3,1,0,
		0,1,5,
		0,5,4,
		1,2,6,
		1,6,5,
		2,3,7,
		2,7,6,
		3,0,4,
		3,4,7,
		4,5,6,
		4,6,7,
	};

	for(size_t i = 0; i < 8; ++i)
	{
		auto j = i * 3;
		verts[j] = verts[j] * sz_x;

		++j;
		verts[j] = verts[j] * sz_y;

		++j;
		verts[j] = verts[j] * sz_z;
	}

	return {
		verts,
		tris
	};
}

void glClearErrors()
{
	GLenum err;
	while((err = glGetError()) != GL_NO_ERROR) continue;
}

}
