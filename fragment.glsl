#version 150 core

uniform sampler2D Tex;
uniform vec4 FillColor;

in vec2 fragTex;

out vec4 color;

void main()
{
	vec2 fragTex2 = vec2(fragTex.s, 1.0f - fragTex.t);
	color = FillColor * texture(Tex, fragTex2);
}
