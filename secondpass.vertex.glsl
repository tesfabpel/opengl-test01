R"===(
#version 330 core

const float chunk_size = 64.0f;

uniform mat4 MV;
uniform mat4 P;
uniform mat4 model_matrix;
uniform float unit_size;

layout(location = 0) in vec3 vpos;

out vec3 pos_local;
out vec4 pos_proj;

void main()
{
	pos_local = vpos;
	pos_proj = P * MV * model_matrix * vec4(vpos, 1);
	
	gl_Position = pos_proj;
}

)==="
