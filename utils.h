//
// Created by tesx on 15/11/16.
//

#ifndef OPENGL_TEST01_UTILS_H
#define OPENGL_TEST01_UTILS_H

#ifdef __cplusplus
extern "C" {
#endif

struct hsv_t {
	float h;
	float s;
	float v;
};

struct rgb_t {
	float r;
	float g;
	float b;
};

struct rgb_t hsv_to_rgb(struct hsv_t hsv);

const char *readAllFile(const char *fn);

void glClearError();

#ifdef __cplusplus
};
#endif

#endif //OPENGL_TEST01_UTILS_H
