R"===(
#version 330 core

const float chunk_size = 64.0f;

uniform mat4 MV;
uniform mat4 P;
uniform mat4 model_matrix;
uniform float unit_size;

layout(location = 0) in vec3 vpos;

out vec3 pos_local;

void main()
{
	pos_local = vpos;
	
	gl_Position = P * MV * model_matrix * vec4(vpos, 1);
}

)==="
