#include "main.h"
#include "gl_renderer.h"
#include "shaders.h"
#include "common.h"

namespace F4SimCore {

gl_renderer *gl_renderer::singleton()
{
	static auto inst = new gl_renderer();
	return inst;
}

gl_renderer::gl_renderer()
	: program_volume_firstpass_()
	, program_volume_secondpass_()
{
	std::vector<const char *> uniforms = {
		RENDERER_UNIFORM_MV_MAT,
		RENDERER_UNIFORM_P_MAT,
		RENDERER_UNIFORM_MODEL_MATRIX,
		RENDERER_UNIFORM_RT_SAMPLER,
		RENDERER_UNIFORM_CHUNK_SAMPLER,
		RENDERER_UNIFORM_STEP_SIZE,
		RENDERER_UNIFORM_UNIT_SIZE,
		RENDERER_UNIFORM_MODE,
		RENDERER_UNIFORM_MAX_N,
	};

	program_volume_firstpass_ = compile_program("volume_prog_firstpass",
		shader_volume_firstpass_vertex_src,
		shader_volume_firstpass_fragment_src,
		uniforms);

	program_volume_secondpass_ = compile_program("volume_prog_secondpass",
		shader_volume_secondpass_vertex_src,
		shader_volume_secondpass_fragment_src,
		uniforms);
}

gl_renderer::~gl_renderer()
{
	if(program_volume_firstpass_ != nullptr)
	{
		glDeleteProgram(program_volume_firstpass_->program_id);
		delete program_volume_firstpass_;
	}

	if(program_volume_secondpass_ != nullptr)
	{
		glDeleteProgram(program_volume_secondpass_->program_id);
		delete program_volume_secondpass_;
	}
}

}
