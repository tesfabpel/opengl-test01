#define GLM_FORCE_RADIANS

#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <epoxy/gl.h>
#include <GLFW/glfw3.h>
#include <vector>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include "ds.h"
#include "utils.h"

//---

GLint ww = 640;
GLint wh = 480;
glm::mat4 mvp(1.0f);

GLuint texDebugId;

//---

void reshape(GLFWwindow *win, int w, int h)
{
	glViewport(0, 0, w, h);
	ww = w;
	wh = h;
}

int main(int argc, char *argv[])
{
	printf("Hello, World!\n");
	
	GLFWwindow *win;
	if(!glfwInit())
		return -1;
	
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);
	//glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	win = glfwCreateWindow(ww, wh, "Hello world :D", NULL, NULL);
	if(!win)
	{
		glfwTerminate();
		return -1;
	}
	
	glfwMakeContextCurrent(win);
	glfwSwapInterval(1);
	glfwSetFramebufferSizeCallback(win, reshape);
	
	char *v = (char *) glGetString(GL_VERSION);
	printf("OpenGL VERSION: %s\n", v);
	
	
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	
	glMatrixMode(GL_TEXTURE);
	glLoadIdentity();
	glm::mat4 txt(1.0f);
	std::cout << glm::to_string(txt) << std::endl;
	txt = glm::scale(txt, glm::vec3(1.0f, -1.0f, 1.0f));
	std::cout << glm::to_string(txt) << std::endl;
	glMultMatrixf(&txt[0][0]);
	glMatrixMode(GL_MODELVIEW);
	
	glEnable(GL_VERTEX_ARRAY);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	
	// Debug Texture
	unsigned char texDebugBuf[] = {
		255, 0, 0, 255,
		0, 255, 0, 255,
		0, 0, 255, 255,
		0, 0, 0, 0,
	};
	glEnable(GL_TEXTURE_2D);
	glGenTextures(1, &texDebugId);
	glBindTexture(GL_TEXTURE_2D, texDebugId);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 2, 2, 0, GL_RGBA, GL_UNSIGNED_BYTE, texDebugBuf);
	glBindTexture(GL_TEXTURE_2D, 0);
	glDisable(GL_TEXTURE_2D);
	
	reshape(win, ww, wh);
	
	while(!glfwWindowShouldClose(win))
	{
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, texDebugId);
		
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);
		
		glColor3f(1.0f, 1.0f, 1.0f);
		
		glBegin(GL_TRIANGLE_STRIP);
		glTexCoord2f(1.0f, 1.0f); glVertex2f(0.75f, 0.75f);
		glTexCoord2f(0.0f, 1.0f); glVertex2f(-0.75f, 0.75f);
		glTexCoord2f(1.0f, 0.0f); glVertex2f(0.75f, -0.75f);
		glTexCoord2f(0.0f, 0.0f); glVertex2f(-0.75f, -0.75f);
		glEnd();
		
		glBindTexture(GL_TEXTURE_2D, 0);
		glDisable(GL_TEXTURE_2D);
		
		glfwSwapBuffers(win);
		glfwPollEvents();
	}
	
	glDeleteTextures(1, &texDebugId);
	
	glfwDestroyWindow(win);
	glfwTerminate();
	return 0;
}
