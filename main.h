//
// Created by tesx on 05/03/17.
//

#ifndef OPENGL_TEST01_MAIN_H
#define OPENGL_TEST01_MAIN_H

#define GLM_FORCE_RADIANS

#include <cstdlib>
#include <iostream>
#include <cmath>
#include <vector>
#include <unordered_map>
#include <type_traits>
#include <glad/glad.h>

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/ext.hpp>

#define N 64
#define C (N*N*N)

struct gl_program
{
	GLuint program_id;
	const char *program_name;
	std::unordered_map<std::string, GLuint> uniforms;
};

struct gl_mesh
{
	std::vector<GLfloat> vertices;
	std::vector<GLushort> triangles;
};

#endif //OPENGL_TEST01_MAIN_H
