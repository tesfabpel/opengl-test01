#include "main.h"
#include <libpng16/png.h>
#include "ds.h"
#include "utils.h"
#include "Entity.h"
#include "TestEntities.h"
#include "Camera.h"
#include "common.h"
#include "gl_renderer.h"

using namespace F4SimCore;

extern "C" {
png_bytep read_png_file(const char *filename);
};

//---

GLuint texGlId;
GLuint texDebugId;
GLuint texClearId;
GLuint texWhiteId;
GLuint tex3dCheckerId;
int ww = 640;
int wh = 480;

tdogl::Camera camera;
glm::vec3 cam_target(32, 32, 32);
glm::vec3 cam_def_pos(-128,128,128);

glm::ivec2 rt_tex_sz_;

int mode = 0;
int max_n = 0;

gl_renderer *r;

bool cull_enabled = false;
bool orbit_enabled;
double orbit_start;

bool mouse_trap = true;

void render_volumetric();

//---

void centerMouse(GLFWwindow *window)
{
	int ww,wh;
	glfwGetWindowSize(window, &ww, &wh);
	glfwSetCursorPos(window, ww/2,wh/2);
}

void scroll_callback(GLFWwindow *window, double xpos, double ypos)
{
	const double scroll_sensitivity = 3.0;
	
	max_n += int(ypos * scroll_sensitivity);
	max_n = glm::clamp(max_n, 0, 256);
}

void cursor_callback(GLFWwindow *window, double xpos, double ypos)
{
	const float mouseSensitivity = 0.125f;
	
	if(!mouse_trap)
		return;
	
	int ww,wh;
	glfwGetWindowSize(window, &ww, &wh);
	
	float xposf = float(xpos) - float(ww/2);
	float yposf = float(ypos) - float(wh/2);
	camera.offsetOrientation(mouseSensitivity * yposf, mouseSensitivity * xposf);
	
	glfwSetCursorPos(window, ww/2,wh/2); //reset the mouse, so it doesn't go out of the window
}

bool maximized = false;
void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods)
{
	static double last_key_cb_time = 0.0;
	double curr_time = glfwGetTime();
	
	if(action == GLFW_PRESS)
	{
		last_key_cb_time = curr_time - 0.5;
	}
	
	float secondsElapsed = float(curr_time - last_key_cb_time);
	last_key_cb_time = curr_time;
	
	const float moveSpeed = 32.0; //units per second
	
	if(action != GLFW_REPEAT
	   && action != GLFW_PRESS)
	{
		return;
	}
	
	switch(key)
	{
		case GLFW_KEY_M:
			mode = (mode+1) % 6;
			break;
			
		case GLFW_KEY_ENTER:
			mouse_trap = !mouse_trap;
			if(mouse_trap) centerMouse(window);
			break;
			
		case GLFW_KEY_A:
			camera.offsetPosition(secondsElapsed * moveSpeed * -camera.right());
			break;
		
		case GLFW_KEY_D:
			camera.offsetPosition(secondsElapsed * moveSpeed * camera.right());
			break;
		
		case GLFW_KEY_W:
			camera.offsetPosition(secondsElapsed * moveSpeed * camera.forward());
			break;
		
		case GLFW_KEY_S:
			camera.offsetPosition(secondsElapsed * moveSpeed * -camera.forward());
			break;
		
		case GLFW_KEY_Q:
			camera.offsetPosition(secondsElapsed * moveSpeed * -camera.up());
			break;
		
		case GLFW_KEY_E:
			camera.offsetPosition(secondsElapsed * moveSpeed * camera.up());
			break;
			
		case GLFW_KEY_O:
			orbit_enabled = !orbit_enabled;
			orbit_start = glfwGetTime();
			camera.setPosition(cam_def_pos);
			break;
			
		case GLFW_KEY_F11:
			maximized = !maximized;
			if(maximized) glfwMaximizeWindow(window);
			else glfwRestoreWindow(window);
			break;
			
		case GLFW_KEY_ESCAPE:
			glfwSetWindowShouldClose(window, true);
			break;
		
		default:
			break;
	}
}

glm::ivec2 vp_size;

void reshape(GLFWwindow *win, int w, int h)
{
	vp_size.x=w;
	vp_size.y=h;
	
	glViewport(0, 0, w, h);
	float wf = (float) w;
	float hf = (float) h;
	
	camera.setViewportAspectRatio(wf / hf);
	
	ww = w;
	wh = h;
}

void render(GLFWwindow *win)
{
	// Clear
	//glClearColor(rgb.r, rgb.g, rgb.b, 1.0);
	glClearColor(0.15f, 0.15f, 0.15f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	if(orbit_enabled)
	{
		double elapsed = glfwGetTime();
		double delta = elapsed - orbit_start;
		// 4s = 360deg = delta : x
		auto angle = (float) (delta * (M_PI * 2.0) / 4.0);
		
		//auto z = camera.position().z;
		
		auto pos = glm::rotateY(cam_def_pos, angle);
		camera.setPosition(pos);
		camera.lookAt(cam_target);
	}
	
	render_volumetric();
	
	
	glfwSwapBuffers(win);
}

GLuint vao_id_;
GLuint vbo_verts_id_;
GLuint vbo_tris_id_;
GLuint vbo_normals_id_;
GLuint rt_fbo_id_;
GLuint rt_tex_id_;
GLuint chunk_texture_id_;

void create_volumetric_cube()
{
	const float usz = 64;
	const auto mesh = create_box(usz, usz, usz);
	
	glGenVertexArrays(1, &vao_id_);
	
	glGenBuffers(1, &vbo_verts_id_);
	glGenBuffers(1, &vbo_tris_id_);
	glGenBuffers(1, &vbo_normals_id_);
	upload_to_vbos(mesh, vao_id_, vbo_verts_id_, vbo_tris_id_);
	
	glGenFramebuffers(1, &rt_fbo_id_);
	
	glBindFramebuffer(GL_FRAMEBUFFER, rt_fbo_id_);
	
	// create render target texture
	glGenTextures(1, &rt_tex_id_);
	
	glBindTexture(GL_TEXTURE_2D, rt_tex_id_);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, rt_tex_id_, 0);
	glBindTexture(GL_TEXTURE_2D, 0);
	
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	
	// create 3d texture
	glGenTextures(1, &chunk_texture_id_);
	
	glBindTexture(GL_TEXTURE_3D, chunk_texture_id_);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	
	const GLint swizzle_mask[] = { GL_RED, GL_RED, GL_RED, GL_RED };
	glTexParameteriv(GL_TEXTURE_3D, GL_TEXTURE_SWIZZLE_RGBA, swizzle_mask);
	
	glTexStorage3D(GL_TEXTURE_3D, 1, GL_R8, N, N, N);
	
	glBindTexture(GL_TEXTURE_3D, 0);
}

const float unit_size_ = 1.0f;
void render_volumetric()
{
	if(rt_tex_sz_ != vp_size)
	{
		// recreate the render target texture
		glBindTexture(GL_TEXTURE_2D, rt_tex_id_);
		glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, vp_size.x, vp_size.y);
		glBindTexture(GL_TEXTURE_2D, 0);
		
		rt_tex_sz_ = vp_size;
	}
	
	
	auto mv_mat = camera.view();
	auto p_mat = camera.projection();
	glm::mat4 chunk_m;
	
	const auto *firstpass = r->program_volume_firstpass();
	const auto *secondpass = r->program_volume_secondpass();
	
	static uint8_t buf[C];
	
	glClearErrors();
	
	glEnable(GL_CULL_FACE);
	
	glBindVertexArray(vao_id_);
	
	// TODO: draw same chunks in groups
	// so that we upload the texture only once
	
	// first pass ---
	{
		const auto &map = firstpass->uniforms;
		
		std::remove_reference_t<decltype(map)>::const_iterator it;
		
		glUseProgram(firstpass->program_id);
		
		it = map.find(RENDERER_UNIFORM_MV_MAT);
		if(it != map.cend())
		{
			glUniformMatrix4fv(it->second, 1, GL_FALSE, &mv_mat[0][0]);
		}
		
		it = map.find(RENDERER_UNIFORM_P_MAT);
		if(it != map.cend())
		{
			glUniformMatrix4fv(it->second, 1, GL_FALSE, &p_mat[0][0]);
		}
		
		it = map.find(RENDERER_UNIFORM_MODEL_MATRIX);
		if(it != map.cend())
		{
			glUniformMatrix4fv(it->second, 1, GL_FALSE, &chunk_m[0][0]);
		}
		
		it = map.find(RENDERER_UNIFORM_UNIT_SIZE);
		if(it != map.cend())
		{
			glUniform1f(it->second, unit_size_);
		}
		
		glBindFramebuffer(GL_FRAMEBUFFER, rt_fbo_id_);
		
		glDisable(GL_DEPTH_TEST);
		
		glClear(GL_COLOR_BUFFER_BIT);
		glDrawBuffer(GL_COLOR_ATTACHMENT0);
		
		glCullFace(GL_FRONT);
		
		auto status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
		auto status_ok = status == GL_FRAMEBUFFER_COMPLETE;
		
		// draw
		glDrawElements(GL_TRIANGLES, 12*3, GL_UNSIGNED_SHORT, nullptr);
		
		glCullFace(GL_BACK);
		
		glEnable(GL_DEPTH_TEST);
		
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
	
	// second pass ---
	{
		const auto &map = secondpass->uniforms;
		
		std::remove_reference_t<decltype(map)>::const_iterator it;
		
		glUseProgram(secondpass->program_id);
		
		it = map.find(RENDERER_UNIFORM_MV_MAT);
		if(it != map.cend())
		{
			glUniformMatrix4fv(it->second, 1, GL_FALSE, &mv_mat[0][0]);
		}
		
		it = map.find(RENDERER_UNIFORM_P_MAT);
		if(it != map.cend())
		{
			glUniformMatrix4fv(it->second, 1, GL_FALSE, &p_mat[0][0]);
		}
		
		it = map.find(RENDERER_UNIFORM_MODEL_MATRIX);
		if(it != map.cend())
		{
			glUniformMatrix4fv(it->second, 1, GL_FALSE, &chunk_m[0][0]);
		}
		
		it = map.find(RENDERER_UNIFORM_STEP_SIZE);
		if(it != map.cend())
		{
			glUniform1f(it->second, unit_size_ / 2.0f);
		}
		
		it = map.find(RENDERER_UNIFORM_UNIT_SIZE);
		if(it != map.cend())
		{
			glUniform1f(it->second, unit_size_);
		}
		
		it = map.find(RENDERER_UNIFORM_MODE);
		if(it != map.cend())
		{
			glUniform1i(it->second, mode);
		}
		
		it = map.find(RENDERER_UNIFORM_MAX_N);
		if(it != map.cend())
		{
			glUniform1i(it->second, max_n);
		}
		
		// upload texture
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_3D, chunk_texture_id_);
		
		for(size_t i = 0; i < C; ++i)
		{
			auto p = to_uintp(i, N);
			
			uint8_t v = 0x00;
			
			if(p.x < 8
			   && p.y < 8
			   && p.z < 8)
			{
				v = 0xFF;
			}
			
			if(p.x == 0
			   && p.y == 0
			   && p.z == 0)
			{
				v = 0x00;
			}
			
			if(p.x == 3
			   && p.y == 0
			   && p.z == 3)
			{
				v = 0x00;
			}
			
			buf[i] = v;
		}
		
		glTexSubImage3D(GL_TEXTURE_3D, 0, 0, 0, 0, N, N, N, GL_RED, GL_UNSIGNED_BYTE, buf);
		
		it = map.find(RENDERER_UNIFORM_CHUNK_SAMPLER);
		if(it != map.cend())
		{
			glUniform1i(it->second, 0);
		}
		
		// render target texture
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, rt_tex_id_);
		
		it = map.find(RENDERER_UNIFORM_RT_SAMPLER);
		if(it != map.cend())
		{
			glUniform1i(it->second, 1);
		}
		
		glDepthFunc(GL_ALWAYS);
		
		// draw
		glDrawElements(GL_TRIANGLES, 12*3, GL_UNSIGNED_SHORT, nullptr);
		
		glDepthFunc(GL_LESS);
		
		glBindTexture(GL_TEXTURE_2D, 0);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_3D, 0);
	}
	
	glUseProgram(0);
	
	glBindVertexArray(0);
	
	glDisable(GL_CULL_FACE);
}

int main(int argc, char *argv[])
{
	printf("Hello, World!\n");
	
	GLFWwindow *win;
	if(!glfwInit())
		return -1;
	
	glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	win = glfwCreateWindow(ww, wh, "Hello world :D", NULL, NULL);
	if(!win)
	{
		glfwTerminate();
		return -1;
	}
	
	//glfwSetInputMode(win, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	int ww,wh;
	glfwGetWindowSize(win, &ww, &wh);
	glfwSetCursorPos(win, ww/2,wh/2);
	
	glfwMakeContextCurrent(win);
	
	if(!gladLoadGL()) {
		printf("Something went wrong!\n");
		exit(-1);
	}
	
	r = gl_renderer::singleton();
	
	char *v = (char *) glGetString(GL_VERSION);
	printf("OpenGL VERSION: %s\n", v);
	
	create_volumetric_cube();
	
	glfwSwapInterval(1);
	glfwSetFramebufferSizeCallback(win, reshape);
	glfwSetKeyCallback(win, key_callback);
	glfwSetCursorPosCallback(win, cursor_callback);
	glfwSetScrollCallback(win, scroll_callback);
	reshape(win, ww, wh);
	
	camera.setPosition(cam_def_pos);
	camera.lookAt(cam_target);
	camera.setNearAndFarPlanes(0.1f, 1000.0f);
	
	while(!glfwWindowShouldClose(win))
	{
		render(win);
		glfwPollEvents();
	}
	
	glfwDestroyWindow(win);
	glfwTerminate();
	return 0;
}
