//
// Created by tesx on 2/28/17.
//

#ifndef OPENGL_TEST01_ENTITY_H
#define OPENGL_TEST01_ENTITY_H

#include "main.h"
#include "ds.h"

class Entity
{
public:
	explicit Entity(GLuint texId) : texId(texId) {};
	
	virtual ~Entity() {};
	
	void onFbResized(int w, int h);
	
	virtual void render(const program_data_t &pd) = 0;
	
	bool ortho = false;

protected:
	int fbw, fbh;
	
	GLuint texId;

private:
	virtual void resized() = 0;
};


#endif //OPENGL_TEST01_ENTITY_H
