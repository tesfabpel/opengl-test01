#ifndef SIMCORE_GL_RENDERER_H
#define SIMCORE_GL_RENDERER_H

#pragma managed(push, off)

#include "main.h"
#include "ds.h"

#define RENDERER_UNIFORM_MV_MAT "MV"
#define RENDERER_UNIFORM_P_MAT "P"
#define RENDERER_UNIFORM_MODEL_MATRIX "model_matrix"
#define RENDERER_UNIFORM_RT_SAMPLER "rt_sampler"
#define RENDERER_UNIFORM_CHUNK_SAMPLER "chunk_sampler"
#define RENDERER_UNIFORM_STEP_SIZE "step_size"
#define RENDERER_UNIFORM_UNIT_SIZE "unit_size"
#define RENDERER_UNIFORM_MODE "mode"
#define RENDERER_UNIFORM_MAX_N "max_n"

namespace F4SimCore {

class gl_renderer
{
public:
	~gl_renderer();

	gl_renderer(const gl_renderer &) = delete;
	gl_renderer(gl_renderer &&) = delete;

	gl_renderer &operator=(const gl_renderer &) = delete;
	gl_renderer &operator=(gl_renderer &&) = delete;

	static gl_renderer *singleton();

	static void init()
	{
		// force creation of singleton
		singleton();
	}

	const gl_program *program_volume_firstpass() const { return program_volume_firstpass_; }
	const gl_program *program_volume_secondpass() const { return program_volume_secondpass_; }

private:
	gl_renderer();

	gl_program *program_volume_firstpass_;
	gl_program *program_volume_secondpass_;
};

}

#pragma managed(pop)

#endif
