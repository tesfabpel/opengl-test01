#version 150 core

uniform mat4 MVP;

in vec3 inPos;

in vec2 vertTex;
out vec2 fragTex;

void main()
{
	fragTex = vertTex;

	gl_Position = MVP * vec4(inPos, 1);
}
