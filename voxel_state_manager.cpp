#include "stdafx.h"
#include "common.h"
#include "voxel_state_manager.h"
#include "gl_renderer.h"

namespace F4SimCore {

struct chunk_cfg
{
	bool all_x;
	bool all_y;
	bool all_z;

	uintp_t start;
	uintp_t end;

	size_t max_c;

	bool is_filled() const { return all_x && all_y && all_z; }

	bool operator==(const chunk_cfg &rhs) const
	{
		return all_x == rhs.all_x
			&& all_y == rhs.all_y
			&& all_z == rhs.all_z
			&& start == rhs.start
			&& end == rhs.end
			&& max_c == rhs.max_c;
	}

	bool operator!=(const chunk_cfg &rhs) const
	{
		return !(*this == rhs);
	}
};

}

namespace std
{
	using namespace F4SimCore;

	template<> 
	struct hash<chunk_cfg>
	{
		typedef chunk_cfg argument_type;
		typedef std::size_t result_type;

		result_type operator()(argument_type const& x) const noexcept
		{
			size_t seed = 0;

			boost::hash_combine(seed, x.all_x);
			boost::hash_combine(seed, x.all_y);
			boost::hash_combine(seed, x.all_z);

			boost::hash_combine(seed, x.start.x);
			boost::hash_combine(seed, x.start.y);
			boost::hash_combine(seed, x.start.z);

			boost::hash_combine(seed, x.end.x);
			boost::hash_combine(seed, x.end.y);
			boost::hash_combine(seed, x.end.z);

			boost::hash_combine(seed, x.max_c);

			return seed;
		}
	};
}

namespace F4SimCore {

voxel_state_manager::voxel_state_manager(float unit_size)
	: unit_size_(unit_size)
	, vao_id_()
	, vbo_verts_id_()
	, vbo_tris_id_()
	, vbo_normals_id_()
	, chunk_texture_id_()
	, rt_fbo_id_()
	, rt_tex_id_()
{
	// create the cube
	const auto usz = float(N) * unit_size;
	const auto mesh = create_box(usz, usz, usz);

	glGenVertexArrays(1, &vao_id_);

	glGenBuffers(1, &vbo_verts_id_);
	glGenBuffers(1, &vbo_tris_id_);
	glGenBuffers(1, &vbo_normals_id_);
	upload_to_vbos(mesh, vao_id_, vbo_verts_id_, vbo_tris_id_, vbo_normals_id_);

	// create render target FB
	glGenFramebuffers(1, &rt_fbo_id_);
	
	glBindFramebuffer(GL_FRAMEBUFFER, rt_fbo_id_);

	// create render target texture
	glGenTextures(1, &rt_tex_id_);

	glBindTexture(GL_TEXTURE_2D, rt_tex_id_);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, rt_tex_id_, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	// create 3d texture
	glGenTextures(1, &chunk_texture_id_);

	glBindTexture(GL_TEXTURE_3D, chunk_texture_id_);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	const GLint swizzle_mask[] = { GL_RED, GL_RED, GL_RED, GL_RED };
	glTexParameteriv(GL_TEXTURE_3D, GL_TEXTURE_SWIZZLE_RGBA, swizzle_mask);

	glTexStorage3D(GL_TEXTURE_3D, 1, GL_R8, N, N, N);

	glBindTexture(GL_TEXTURE_3D, 0);
}

voxel_state_manager::~voxel_state_manager()
{
	glDeleteVertexArrays(1, &vao_id_);

	glDeleteBuffers(1, &vbo_normals_id_);
	glDeleteBuffers(1, &vbo_tris_id_);
	glDeleteBuffers(1, &vbo_verts_id_);

	glDeleteTextures(1, &chunk_texture_id_);

	glDeleteFramebuffers(1, &rt_fbo_id_);
	glDeleteTextures(1, &rt_tex_id_);
}

bool voxel_state_manager::get(const intp_t &p) const
{
	const auto t = point_to_key_and_index(p);
	const auto k = std::get<0>(t);
	const auto i = uint32_t(std::get<1>(t));

	const auto ci = state_.find(k);
	if(ci == state_.end())
	{
		return false;
	}

	const auto &chunk = ci->second;
	
	_ASSERT(bool(chunk));
	
	return chunk->get(i);
}

void voxel_state_manager::set(const intp_t &p, bool v)
{
	const auto t = point_to_key_and_index(p);
	const auto k = std::get<0>(t);
	const auto i = uint32_t(std::get<1>(t));

	auto s2 = state_;

	auto &chunk = s2[k];

	_ASSERT(bool(chunk));

	auto *chunk_new = new chunk_t(*chunk);
	chunk_new->set(i, v);

	s2[k] = std::shared_ptr<const chunk_t>(chunk_new);

	state_ = s2;
}

bool voxel_state_manager::get(const glm::vec3 &p) const
{
	const auto ip = to_intp(p, this->unit_size_);
	return get(ip);
}

void voxel_state_manager::set(const glm::vec3 &p, bool v)
{
	const auto ip = to_intp(p, this->unit_size_);
	set(ip, v);
}

size_t voxel_state_manager::multi_set(const std::vector<intp_t> &points, 
	bool v)
{
	size_t changeds = 0u;

	if(points.empty()) 
		return 0u;

	std::unordered_map<chunk_key, std::vector<uint32_t>> bag;
	for(const auto &ip : points)
	{
		const auto t = point_to_key_and_index(ip);
		const auto k = std::get<0>(t);
		const auto i = uint32_t(std::get<1>(t));

		auto &vec = bag[k];
		vec.push_back(i);
	}

	auto s2 = state_;

	//std::vector<chunk_key> changeds;
	for(const auto &t : bag)
	{
		const auto &k = t.first;
		const auto &indices = t.second;

		chunk_t *chunk_new;

		auto &chunk_ptr = s2[k];
		if(!chunk_ptr)
		{
			// the chunk doesn't exist
			if(v)
			{
				chunk_new = new chunk_t();
			}
			else 
			{
				// don't remove anything on empty chunk
				continue;
			}
		}
		else
		{
			const auto &chunk_old = *chunk_ptr;

			// don't add anything on filled chunk
			if(chunk_old.is_filled() && v) 
			{
				continue;
			}

			// don't remove anything on empty chunk
			if(roaring_bitmap_is_empty(chunk_old.pointer_const()) && !v)
			{
				continue;
			}

			chunk_new = new chunk_t(chunk_old);
		}

		auto *rr = chunk_new->pointer();

		const size_t n = indices.size();
		const uint32_t *data = indices.data();

		auto *rr_old = roaring_bitmap_copy(rr);

		if(v)
		{
			roaring_bitmap_add_many(rr, n, data);
		}
		else
		{
			auto *rr2 = roaring_bitmap_create();
			roaring_bitmap_add_many(rr2, n, data);
			
			roaring_bitmap_andnot_inplace(rr, rr2);
			
			roaring_bitmap_free(rr2);
		}

		s2[k] = std::shared_ptr<const chunk_t>(chunk_new);

		const bool changed = roaring_bitmap_equals(rr, rr_old);
		if(changed)
			changeds++;

		roaring_bitmap_free(rr_old);
	}

	state_ = s2;

	return changeds;
}

size_t voxel_state_manager::cut(bounds_t panel_bounds,
	const glm::mat4 &panel_xform, 
	const std::vector<std::pair<curr_subtool_data, glm::mat4>> &cuts)
{
	if(cuts.empty())
		return 0u;

	panel_bounds.transform(panel_xform);

	std::vector<glm::vec3> points;
	for(const auto &cut : cuts)
	{
		const auto &sd = cut.first;
		const auto &xform = cut.second;

		// rough check of panel-tool intersection
		const auto tool_bounds = sd.bounds.transformed(xform);

		if(!panel_bounds.intersects(tool_bounds))
		{
			continue;
		}

		// apply the xform to the points and add them to points
		points.reserve(points.size() + sd.vertices.size());
		for(const auto &v : sd.vertices)
		{
			const glm::vec3 v2 = xform * glm::vec4(v, 1.0f);

			// check if v2 is inside panel bounds
			if(!panel_bounds.is_inside(v2))
				continue;

			points.push_back(v2);
		}
	}

	const float usz = unit_size();

	// convert points to int points
	std::vector<intp_t> ips;
	ips.reserve(points.size());
	
	for(const auto &v : points)
	{
		ips.push_back(to_intp(v, usz));
	}

	// cut
	const size_t changeds = multi_set(ips, false);
	return unsigned int(changeds);
}

void voxel_state_manager::fill(const glm::vec3 &min, const glm::vec3 &max, bool v)
{
	const auto &minv = to_intp(min, this->unit_size_);
	const auto &maxv = to_intp(max, this->unit_size_);

	if(maxv.x < minv.x) return;
	if(maxv.y < minv.y) return;
	if(maxv.z < minv.z) return;

	const auto &mink = chunk_key::from_intp(minv);
	const auto &maxk = chunk_key::from_intp(maxv);

	const auto &chunk_keys = chunk_key::get_keys_in_range(mink, maxk);
	
	if(chunk_keys.empty()) 
		return;

	auto s2 = state_;

	std::shared_ptr<chunk_t> filled;
	if(v)
	{
		filled.reset(new chunk_t(chunk_t::create_filled()));
	}

	std::unordered_map<chunk_cfg, std::vector<chunk_key>> configs;
	for(const auto &k : chunk_keys)
	{
		const auto &cmin = k.min_point();
		const auto &cmax = k.max_point();

		// get start and end points
		const intp_t abs_start = 
		{
			std::max(minv.x, cmin.x),
			std::max(minv.y, cmin.y),
			std::max(minv.z, cmin.z)
		};

		const intp_t abs_end = 
		{
			std::min(maxv.x, cmax.x),
			std::min(maxv.y, cmax.y),
			std::min(maxv.z, cmax.z)
		};

		// can optimize fill
		if(abs_start == cmin
			&& abs_end == cmax)
		{
			chunk_cfg cfg {};
			cfg.all_x = true;
			cfg.all_y = true;
			cfg.all_z = true;
			cfg.start = { 0, 0, 0 };
			cfg.end = { N-1, N-1, N-1 };
			cfg.max_c = C;

			configs[cfg].push_back(k);
			continue;
		}

		// get deltas
		const auto dx = size_t(abs_end.x - abs_start.x);
		const auto dy = size_t(abs_end.y - abs_start.y);
		const auto dz = size_t(abs_end.z - abs_start.z);
		const auto maxc((dx + 1) * (dy + 1) * (dz + 1));

		// get chunk-relative start and end points
		const uintp_t start = 
		{
			size_t(abs_start.x - cmin.x),
			size_t(abs_start.y - cmin.y),
			size_t(abs_start.z - cmin.z)
		};

		const uintp_t end = 
		{
			start.x + dx,
			start.y + dy,
			start.z + dz
		};

		// assertions for start
		_ASSERT(start.x < N);
		_ASSERT(start.y < N);
		_ASSERT(start.z < N);

		// assertions for end
		_ASSERT(end.x < N);
		_ASSERT(end.y < N);
		_ASSERT(end.z < N);

		const bool all_x = start.x == 0
			&& end.x == N - 1;

		const bool all_y = start.y == 0
			&& end.y == N - 1;

		const bool all_z = start.z == 0
			&& end.z == N - 1;

		chunk_cfg cfg {};
		cfg.all_x = all_x;
		cfg.all_y = all_y;
		cfg.all_z = all_z;
		cfg.start = start;
		cfg.end = end;
		cfg.max_c = maxc;

		configs[cfg].push_back(k);
	}

	const size_t configs_sz = configs.size();

	std::vector<std::pair<chunk_cfg, std::vector<chunk_key>>> vec(configs_sz);
	std::move(configs.begin(), configs.end(), vec.begin());

	const auto &configs_begin = vec.cbegin();

	std::vector<std::pair<chunk_key, std::shared_ptr<chunk_t>>> changeds;

	const auto &gnao = s2;

	#pragma omp parallel
	{
		std::vector<std::pair<chunk_key, std::shared_ptr<chunk_t>>> local;

		#pragma omp parallel for
		for(int i = 0; i < int(configs_sz); ++i)
		{
			const auto &pair = *(configs_begin + i);
			const auto &cfg = pair.first;
			const auto &keys = pair.second;

			// create the chunk template for this config
			std::shared_ptr<chunk_t> chunk;

			if(cfg.is_filled())
			{
				chunk = filled;
			}
			else
			{
				std::vector<uint32_t> indices;
				indices.reserve(cfg.max_c);

				for(auto z = cfg.start.z; z <= cfg.end.z; ++z)
				for(auto y = cfg.start.y; y <= cfg.end.y; ++y)
				for(auto x = cfg.start.x; x <= cfg.end.x; ++x)
				{
					const auto j = uint32_t(to_index({ x, y, z }));
					indices.push_back(j);
				}

				auto *rr = roaring_bitmap_create();
				roaring_bitmap_add_many(rr, indices.size(), indices.data());

				chunk.reset(new chunk_t(rr, false));
			}

			// ...
			for(const auto &key : keys)
			{
				// the new chunk is filled, just return it
				if(cfg.is_filled())
				{
					local.emplace_back(key, chunk);
					continue;
				}

				// does a chunk already exist with the same key?
				const auto existingi = gnao.find(key);
				if(existingi == gnao.end())
				{
					// no, just return it
					local.emplace_back(key, chunk);
					continue;
				}

				auto chunk_old_ptr = existingi->second;
				const auto &chunk_old = chunk_old_ptr.get();

				// apply the new chunk to the existing chunk
				auto *rr = chunk->pointer();
				auto *rr_old = chunk_old->pointer_const();

				auto *rr_new = roaring_bitmap_set(rr_old, rr, v);
				auto *chunk_new = new chunk_t(rr_new, false);

				local.emplace_back(key, chunk_new);
			}
		}

		#pragma omp critical
		changeds.insert(changeds.cend(),
			std::make_move_iterator(local.begin()),
			std::make_move_iterator(local.end()));
	}

	for(auto &c : changeds)
	{
		s2[c.first] = std::move(c.second);
	}

	state_ = s2;
}

void voxel_state_manager::render(const glm::ivec2 &vp_size,
	const glm::mat4 &mv_mat, 
	const glm::mat4 &p_mat)
{
	const auto r = gl_renderer::singleton();

	if(rt_tex_sz_ != vp_size)
	{
		// recreate the render target texture
		glBindTexture(GL_TEXTURE_2D, rt_tex_id_);
		glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, vp_size.x, vp_size.y);
		glBindTexture(GL_TEXTURE_2D, 0);

		rt_tex_sz_ = vp_size;
	}

	r->start_rendering();
	
	const auto *firstpass = r->program_volume_firstpass();
	const auto *secondpass = r->program_volume_secondpass();

	static uint8_t buf[C];

	glClearErrors();

	glEnable(GL_CULL_FACE);

	glBindVertexArray(this->vao_id_);

	const auto state = state_;
	for(const auto &kvp : state)
	{
		// TODO: draw same chunks in groups
		// so that we upload the texture only once

		const auto &k = kvp.first;
		const auto &v = kvp.second;
		const auto *chunk = v.get();

		const auto mp = k.min_point();
		const auto mp3d = to_p3d(mp, unit_size_);

		const auto chunk_m = glm::translate(glm::mat4(), mp3d);

		// first pass ---
		{
			const auto &map = firstpass->uniforms;

			std::remove_reference_t<decltype(map)>::const_iterator it;

			glUseProgram(firstpass->program_id);

			it = map.find(RENDERER_UNIFORM_MV_MAT);
			if(it != map.cend())
			{
				glUniformMatrix4fv(it->second, 1, GL_FALSE, &mv_mat[0][0]);
			}

			it = map.find(RENDERER_UNIFORM_P_MAT);
			if(it != map.cend())
			{
				glUniformMatrix4fv(it->second, 1, GL_FALSE, &p_mat[0][0]);
			}

			it = map.find(RENDERER_UNIFORM_MODEL_MATRIX);
			if(it != map.cend())
			{
				glUniformMatrix4fv(it->second, 1, GL_FALSE, &chunk_m[0][0]);
			}

			it = map.find(RENDERER_UNIFORM_UNIT_SIZE);
			if(it != map.cend())
			{
				glUniform1f(it->second, unit_size_);
			}
			
			glBindFramebuffer(GL_FRAMEBUFFER, rt_fbo_id_);

			glDisable(GL_DEPTH_TEST);

			glClear(GL_COLOR_BUFFER_BIT);
			glDrawBuffer(GL_COLOR_ATTACHMENT0);

			glCullFace(GL_FRONT);

			auto status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
			auto status_ok = status == GL_FRAMEBUFFER_COMPLETE;
		
			// draw
			glDrawElements(GL_TRIANGLES, 12*3, GL_UNSIGNED_SHORT, nullptr);

			glCullFace(GL_BACK);

			glEnable(GL_DEPTH_TEST);

			glBindFramebuffer(GL_FRAMEBUFFER, 0);
		}
		
		// second pass ---
		{
			const auto &map = secondpass->uniforms;
			
			std::remove_reference_t<decltype(map)>::const_iterator it;
			
			glUseProgram(secondpass->program_id);

			it = map.find(RENDERER_UNIFORM_MV_MAT);
			if(it != map.cend())
			{
				glUniformMatrix4fv(it->second, 1, GL_FALSE, &mv_mat[0][0]);
			}

			it = map.find(RENDERER_UNIFORM_P_MAT);
			if(it != map.cend())
			{
				glUniformMatrix4fv(it->second, 1, GL_FALSE, &p_mat[0][0]);
			}

			it = map.find(RENDERER_UNIFORM_MODEL_MATRIX);
			if(it != map.cend())
			{
				glUniformMatrix4fv(it->second, 1, GL_FALSE, &chunk_m[0][0]);
			}

			it = map.find(RENDERER_UNIFORM_STEP_SIZE);
			if(it != map.cend())
			{
				glUniform1f(it->second, unit_size_ / 2.0f);
			}

			it = map.find(RENDERER_UNIFORM_UNIT_SIZE);
			if(it != map.cend())
			{
				glUniform1f(it->second, unit_size_);
			}
		
			// upload texture
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_3D, chunk_texture_id_);
		
			const auto &indices = chunk->get_indices();
			for(size_t i = 0; i < C; ++i)
			{
				buf[i] = 0;
			}
			for(const auto &i : indices)
			{
				buf[i] = 0xFF;
			}

			glTexSubImage3D(GL_TEXTURE_3D, 0, 0, 0, 0, N, N, N, GL_RED, GL_UNSIGNED_BYTE, buf);

			it = map.find(RENDERER_UNIFORM_CHUNK_SAMPLER);
			if(it != map.cend())
			{
				glUniform1i(it->second, 0);
			}

			// render target texture
			glActiveTexture(GL_TEXTURE1);
			glBindTexture(GL_TEXTURE_2D, rt_tex_id_);

			it = map.find(RENDERER_UNIFORM_RT_SAMPLER);
			if(it != map.cend())
			{
				glUniform1i(it->second, 1);
			}
			
			glDepthFunc(GL_ALWAYS);

			// draw
			glDrawElements(GL_TRIANGLES, 12*3, GL_UNSIGNED_SHORT, nullptr);
			
			glDepthFunc(GL_LESS);

			glBindTexture(GL_TEXTURE_2D, 0);
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_3D, 0);
		}
	}

	glUseProgram(0);

	glBindVertexArray(0);

	glDisable(GL_CULL_FACE);

	r->stop_rendering();
}

}
