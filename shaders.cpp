#include "main.h"
#include "shaders.h"

const char *shader_volume_firstpass_vertex_src =
#include "firstpass.vertex.glsl"
;

const char *shader_volume_firstpass_fragment_src =
#include "firstpass.fragment.glsl"
;

const char *shader_volume_secondpass_vertex_src =
#include "secondpass.vertex.glsl"
;

const char *shader_volume_secondpass_fragment_src =
#include "secondpass.fragment.glsl"
;
