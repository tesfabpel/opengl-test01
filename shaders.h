#ifndef SIMCORE_SHADERS_H
#define SIMCORE_SHADERS_H

#pragma managed(push, off)

extern const char *shader_test_vertex_src;
extern const char *shader_test_fragment_src;

extern const char *shader_volume_firstpass_vertex_src;
extern const char *shader_volume_firstpass_fragment_src;

extern const char *shader_volume_secondpass_vertex_src;
extern const char *shader_volume_secondpass_fragment_src;

#pragma managed(pop)

#endif
