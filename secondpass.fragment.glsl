R"===(
#version 330 core

const float chunk_size = 64.0f;

const float M_PI = 3.14159265359f;
const float M_PI_2 = M_PI / 2.0f;
const float M_PI_4 = M_PI / 4.0f;

const float FLT_MAX = 3.402823e+38f;

//const int MODE_DEFAULT = 0; // or other values
const int MODE_VISITED_VOXELS = 1;
const int MODE_FRONT = 2;
const int MODE_BACK = 3;
const int MODE_RAY_LENGTH = 4;
const int MODE_LAST_VOXEL = 5;

uniform mat4 MV;
uniform mat4 P;
uniform mat4 model_matrix;
uniform usampler3D chunk_sampler;
uniform sampler2D rt_sampler;
uniform float step_size;
uniform float unit_size;
uniform int mode;
uniform int max_n;

in vec3 pos_local;
in vec4 pos_proj;

out vec4 color;

struct MyDebug
{
	float alpha;
	float visited;
	bool clamped;
	vec3 last_voxel;
};

struct TexAccess
{
	float v;
	bool clamped;
};

TexAccess visitVoxel(usampler3D tex, vec3 pos)
{
	vec3 texcoord = pos / chunk_size;
	float v = texture(tex, texcoord).a;

	bool clamped = texcoord.x > 1.0f || texcoord.x < 0.0f
		|| texcoord.y > 1.0f || texcoord.y < 0.0f
		|| texcoord.z > 1.0f || texcoord.z < 0.0f;
	return TexAccess(v, clamped);
}

MyDebug myTexture(usampler3D tex, 
	vec3 ray_start,
	vec3 ray_end)
{
	// https://github.com/francisengelmann/fast_voxel_traversal/blob/master/main.cpp
	
	/*
	// hard-limit
	{
		vec3 dir = ray_end - ray_start;
		float dir_len = min(length(dir), max_ray_length);
		vec3 dir2 = normalize(dir) * dir_len;
		ray_end = ray_start + dir2;
	}
	*/
	
	float v = 0.0f;
	int done = 0;
	bool clamped = false;
	
	ivec3 current_voxel = ivec3(floor(ray_start));
	ivec3 last_voxel = ivec3(floor(ray_end));
	
	if(current_voxel == last_voxel)
	{
		TexAccess ta = visitVoxel(tex, current_voxel);
		v = ta.v;
		clamped = ta.clamped;
		done++;
	}
	else
	{
		vec3 ray = ray_end - ray_start;
		ivec3 step;
		{
			int x = (ray.x >= 0.0f) ? 1 : -1;
			int y = (ray.y >= 0.0f) ? 1 : -1;
			int z = (ray.z >= 0.0f) ? 1 : -1;
			step = ivec3(x, y, z);
		}
		
		vec3 next_voxel_boundary = vec3(current_voxel + step);
		
		float tMaxX = (abs(ray.x) >= 1e-9f) ? (next_voxel_boundary.x - ray_start.x) / ray.x : FLT_MAX;
		float tMaxY = (abs(ray.y) >= 1e-9f) ? (next_voxel_boundary.y - ray_start.y) / ray.y : FLT_MAX;
		float tMaxZ = (abs(ray.z) >= 1e-9f) ? (next_voxel_boundary.z - ray_start.z) / ray.z : FLT_MAX;
		
		float tDeltaX = (abs(ray.x) >= 1e-9f) ? 1.0f / ray.x * float(step.x) : FLT_MAX;
		float tDeltaY = (abs(ray.y) >= 1e-9f) ? 1.0f / ray.y * float(step.y) : FLT_MAX;
		float tDeltaZ = (abs(ray.z) >= 1e-9f) ? 1.0f / ray.z * float(step.z) : FLT_MAX;
		
		ivec3 diff = ivec3(0);
		bool neg_ray = false;
		if(current_voxel.x != last_voxel.x && ray.x < 0.0f) { diff.x--; neg_ray = true; }
		if(current_voxel.y != last_voxel.y && ray.y < 0.0f) { diff.y--; neg_ray = true; }
		if(current_voxel.z != last_voxel.z && ray.z < 0.0f) { diff.z--; neg_ray = true; }
		if(neg_ray)
		{
			TexAccess ta = visitVoxel(tex, current_voxel);
			v += ta.v;
			clamped = clamped || ta.clamped;

			done++;

			current_voxel += diff;
		}
		
		int n = int(ceil(chunk_size * sqrt(3))) * 2;
		if(max_n != 0)
		{
			n = min(max_n, n);
		}

		do
		{
			TexAccess ta = visitVoxel(tex, current_voxel);
			v += ta.v;
			clamped = clamped || ta.clamped;
			
			done++;
			
			if(v >= 1.0f)
				break;
			
			//...
			
			if(tMaxX < tMaxY) 
			{
				if(tMaxX < tMaxZ)
				{
					current_voxel.x += step.x;
					tMaxX += tDeltaX;
				}
				else
				{
					current_voxel.z += step.z;
					tMaxZ += tDeltaZ;
				}
			}
			else
			{
				if(tMaxY < tMaxZ)
				{
					current_voxel.y += step.y;
					tMaxY += tDeltaY;
				}
				else
				{
					current_voxel.z += step.z;
					tMaxZ += tDeltaZ;
				}
			}
		}
		while(last_voxel != current_voxel && n-- > 0);
	}
	
	v = min(v, 1.0f); 
	float l = 1.0f - min(max(float(done) / (chunk_size * sqrt(3.0f)), 0.0f), 1.0f);

	MyDebug ret;
	ret.alpha = v;
	ret.visited = l;
	ret.clamped = clamped;
	ret.last_voxel = current_voxel;

	return ret;
}

void main()
{
	// transform the coordinates it from [-1;1] to [0;1]
	vec2 texc = vec2(((pos_proj.x / pos_proj.w) + 1.0 ) / 2.0,
	                 ((pos_proj.y / pos_proj.w) + 1.0 ) / 2.0);
	
	// back_pos and front_pos are in range 0..64

	// the back position is the world space 
	// position stored in the texture...
	vec4 rt_color = texture(rt_sampler, texc);
	if(abs(rt_color.a - 1.0f) > 0.01f)
		discard;

	vec3 back_pos = floor(rt_color.xyz * chunk_size);

	if(mode == MODE_BACK)
	{
		color = vec4(floor(back_pos) / chunk_size, 1.0f);
		return;
	}
	
	// the front position is the world space position of the second render pass.
	vec3 front_pos = floor(pos_local / unit_size);

	if(mode == MODE_FRONT)
	{
		color = vec4(floor(front_pos) / chunk_size, 1.0f);
		return;
	}

	// the direction from the front position to back position.
	vec3 dir = back_pos - front_pos;
	
	float ray_length = length(dir);
	ray_length = min(ray_length, chunk_size * sqrt(3)); // hard limit
	
	if(mode == MODE_RAY_LENGTH)
	{
		color = vec4(ray_length) / (chunk_size * sqrt(3));
		color.a = 1.0f;
		return;
	}
	
	//float alpha = myTexture(chunk_sampler, front_pos, back_pos, ray_length);
	MyDebug debug = myTexture(chunk_sampler, front_pos, back_pos);
	
	float l = debug.visited;
	float alpha = debug.alpha;
	bool clamped = debug.clamped;
	vec3 last_voxel = debug.last_voxel;

	if(mode == MODE_LAST_VOXEL)
	{
		color = vec4(last_voxel / (chunk_size*2), 1.0f);
		return;
	}

	if(mode == MODE_VISITED_VOXELS)
	{
		if(clamped)
		{
			color = vec4(1, 0, 0, 1);
			return;
		}

		if(alpha < 0.1f) color = vec4(0, l, 0, 1);
		else color = vec4(l, l, l, 1);

		return;
	}

	if(alpha < 0.1f) 
		discard;

	color = vec4(1);
}

)==="
