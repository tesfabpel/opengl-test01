//
// Created by tesx on 2/28/17.
//

#include "TestEntities.h"

GlLogo::GlLogo(GLuint texId)
	: Entity(texId)
{
	glGenBuffers(1, &vbo_id);
	glGenBuffers(1, &vbo_tex_id);
}

GlLogo::~GlLogo()
{
	glDeleteBuffers(1, &vbo_tex_id);
	glDeleteBuffers(1, &vbo_id);
}

void GlLogo::resized()
{
	static const GLfloat verts[] = {
		1.0f, 1.0f, 0.0f,
		-1.0f, 1.0f, 0.0f,
		1.0f, -1.0f, 0.0f,
		-1.0f, -1.0f, 0.0f,
	};
	
	static const GLfloat uvs[] = {
		1.0f, 1.0f,
		0.0f, 1.0f,
		1.0f, 0.0f,
		0.0f, 0.0f,
	};
	
	// Upload vertices
	glBindBuffer(GL_ARRAY_BUFFER, vbo_id);
	glBufferData(GL_ARRAY_BUFFER, sizeof(verts), verts, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	// Upload UVs
	glBindBuffer(GL_ARRAY_BUFFER, vbo_tex_id);
	glBufferData(GL_ARRAY_BUFFER, sizeof(uvs), uvs, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void GlLogo::render(const program_data_t &pd)
{
	glBindTexture(GL_TEXTURE_2D, texId);
	
	glEnableVertexAttribArray((GLuint) pd.vertex_a);
	glEnableVertexAttribArray((GLuint) pd.uv_a);
	
	// Vertices
	glBindBuffer(GL_ARRAY_BUFFER, vbo_id);
	glVertexAttribPointer(
		(GLuint) pd.vertex_a,  // attribute id
		3,                     // size
		GL_FLOAT,              // type
		GL_FALSE,              // normalized?
		0,                     // stride
		(void *)0              // array buffer offset
	);
	
	// UVs
	glBindBuffer(GL_ARRAY_BUFFER, vbo_tex_id);
	glVertexAttribPointer(
		(GLuint) pd.uv_a,      // attribute id
		2,                     // size
		GL_FLOAT,              // type
		GL_FALSE,              // normalized?
		0,                     // stride
		(void *)0              // array buffer offset
	);
	
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glDisableVertexAttribArray((GLuint) pd.uv_a);
	glDisableVertexAttribArray((GLuint) pd.vertex_a);
	
	glBindTexture(GL_TEXTURE_2D, 0);
}
