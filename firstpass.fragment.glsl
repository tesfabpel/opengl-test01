R"===(
#version 330 core

const float chunk_size = 64.0f;

uniform mat4 MV;
uniform mat4 P;
uniform mat4 model_matrix;
uniform float step_size;
uniform float unit_size;

in vec3 pos_local;

out vec4 color;

void main()
{
	color = vec4(pos_local / unit_size / chunk_size, 1);
}

)==="
