#include "utils.h"

#include <math.h>
#include <glib.h>
#include <glad/glad.h>

struct rgb_t hsv_to_rgb(struct hsv_t hsv)
{
	float fH = hsv.h * 360.0f;
	float fS = hsv.s;
	float fV = hsv.v;
	
	float fR;
	float fG;
	float fB;
	
	float fC = fV * fS; // Chroma
	float fHPrime = fmodf(fH / 60.0f, 6);
	float fX = fC * (1 - fabsf(fmodf(fHPrime, 2) - 1));
	float fM = fV - fC;
	
	if(0 <= fHPrime && fHPrime < 1)
	{
		fR = fC;
		fG = fX;
		fB = 0;
	}
	else if(1 <= fHPrime && fHPrime < 2)
	{
		fR = fX;
		fG = fC;
		fB = 0;
	}
	else if(2 <= fHPrime && fHPrime < 3)
	{
		fR = 0;
		fG = fC;
		fB = fX;
	}
	else if(3 <= fHPrime && fHPrime < 4)
	{
		fR = 0;
		fG = fX;
		fB = fC;
	}
	else if(4 <= fHPrime && fHPrime < 5)
	{
		fR = fX;
		fG = 0;
		fB = fC;
	}
	else if(5 <= fHPrime && fHPrime < 6)
	{
		fR = fC;
		fG = 0;
		fB = fX;
	}
	else
	{
		fR = 0;
		fG = 0;
		fB = 0;
	}
	
	fR += fM;
	fG += fM;
	fB += fM;
	
	return (struct rgb_t) { fR, fG, fB };
}

const char *readAllFile(const char *fn)
{
	gchar *contents;
	gsize length;
	GError *err;
	gboolean ok = g_file_get_contents(fn, &contents, &length, &err);
	if(!ok) return NULL;
	
	return contents;
}

void glClearError()
{
	GLenum err;
	do
	{
		err = glGetError();
	}
	while(err != GL_NO_ERROR);
}
