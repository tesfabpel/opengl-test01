//
// Created by tesx on 03/03/17.
//

#include "TestEntities.h"
#include "utils.h"

ColorStripe::ColorStripe(GLuint texId)
	: Entity(texId)
{
	glGenBuffers(1, &vbo_triangle_id);
	glGenBuffers(1, &vbo_left_stripe_id);
	glGenBuffers(1, &vbo_right_stripe_id);
	glGenBuffers(1, &vbo_tex_id);
	
	ortho = true;
}

ColorStripe::~ColorStripe()
{
	glDeleteBuffers(1, &vbo_tex_id);
	glDeleteBuffers(1, &vbo_right_stripe_id);
	glDeleteBuffers(1, &vbo_left_stripe_id);
	glDeleteBuffers(1, &vbo_triangle_id);
}

void ColorStripe::resized()
{
	float ww = fbw;
	float wh = fbh;
	float wwh = ww / 2.0f;
	float whh = wh / 2.0f;
	
	float trSz = triangleSize;
	float distance = stripeDistance;
	
	// Center Triangle
	float center[] = {
		trSz, -whh, 0.0f,
		0.0f, -whh + trSz, 0.0f,
		-trSz, -whh, 0.0f,
	};
	
	glBindBuffer(GL_ARRAY_BUFFER, vbo_triangle_id);
	glBufferData(GL_ARRAY_BUFFER, sizeof(center), center, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	// Left Stripe
	float left[] = {
		-distance, -whh + trSz, 0.0f,
		-wwh, -whh + trSz, 0.0f,
		-trSz - distance, -whh, 0.0f,
		-wwh, -whh, 0.0f,
	};
	
	glBindBuffer(GL_ARRAY_BUFFER, vbo_left_stripe_id);
	glBufferData(GL_ARRAY_BUFFER, sizeof(left), left, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	// Right Stripe
	float right[] = {
		wwh, -whh + trSz, 0.0f,
		distance, -whh + trSz, 0.0f,
		wwh, -whh, 0.0f,
		trSz + distance, -whh, 0.0f,
	};
	
	glBindBuffer(GL_ARRAY_BUFFER, vbo_right_stripe_id);
	glBufferData(GL_ARRAY_BUFFER, sizeof(right), right, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void ColorStripe::render(const program_data_t &pd)
{
	double elapsed = glfwGetTime();
	
	float hue = 0.0f;
	float step = 1.0f / 10.0f;
	
	// Calculate bg color
	hue += step * elapsed;
	hue = fmodf(hue, 1.0f);
	
	struct rgb_t rgb = hsv_to_rgb((struct hsv_t) { hue, 1.0, 1.0 });
	glm::vec4 color(rgb.r, rgb.g, rgb.b, 1.0f);
	glUniform4fv(pd.color_u, 1, &color[0]);
	
	glBindTexture(GL_TEXTURE_2D, texId);
	glEnableVertexAttribArray((GLuint) pd.vertex_a);
	
	// Center Triangle
	glBindBuffer(GL_ARRAY_BUFFER, vbo_triangle_id);
	glVertexAttribPointer(
		(GLuint) pd.vertex_a,  // attribute id
		3,                     // size
		GL_FLOAT,              // type
		GL_FALSE,              // normalized?
		0,                     // stride
		(void *) 0              // array buffer offset
	);
	
	glDrawArrays(GL_TRIANGLES, 0, 3);
	
	glEnable(GL_SCISSOR_TEST);
	glScissor(0, 0, fbw, (int) (triangleSize * 0.75));
	
	// Left Stripe
	glBindBuffer(GL_ARRAY_BUFFER, vbo_left_stripe_id);
	glVertexAttribPointer(
		(GLuint) pd.vertex_a,  // attribute id
		3,                     // size
		GL_FLOAT,              // type
		GL_FALSE,              // normalized?
		0,                     // stride
		(void *) 0              // array buffer offset
	);

	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	// Right Stripe
	glBindBuffer(GL_ARRAY_BUFFER, vbo_right_stripe_id);
	glVertexAttribPointer(
		(GLuint) pd.vertex_a,  // attribute id
		3,                     // size
		GL_FLOAT,              // type
		GL_FALSE,              // normalized?
		0,                     // stride
		(void *) 0              // array buffer offset
	);

	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	glDisable(GL_SCISSOR_TEST);
	
	glDisableVertexAttribArray((GLuint) pd.vertex_a);
	glBindTexture(GL_TEXTURE_2D, 0);
}
