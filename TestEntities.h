//
// Created by tesx on 2/28/17.
//

#ifndef OPENGL_TEST01_TESTENTITIES_H
#define OPENGL_TEST01_TESTENTITIES_H

#include "main.h"
#include "Entity.h"

class GlLogo : public Entity
{
public:
	explicit GlLogo(GLuint texId);
	~GlLogo();
	
	void render(const program_data_t &pd);

private:
	void resized();
	
	GLuint vbo_id;
	GLuint vbo_tex_id;
};

class ColorStripe : public Entity
{
public:
	explicit ColorStripe(GLuint texId);
	~ColorStripe();
	
	void render(const program_data_t &pd);
	
	GLfloat triangleSize;
	GLfloat stripeDistance;

private:
	void resized();
	
	GLuint vbo_triangle_id;
	GLuint vbo_left_stripe_id;
	GLuint vbo_right_stripe_id;
	GLuint vbo_tex_id;
};


#endif //OPENGL_TEST01_TESTENTITIES_H
